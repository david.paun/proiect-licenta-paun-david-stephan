using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkinSelection_UI : MonoBehaviour
{
    [SerializeField] private int[] priceForSkin;

    [SerializeField] private bool[] skinPurchased;
    private int skin_Id;

    [Header("Components")]
    [SerializeField] private TextMeshProUGUI bankText;
    [SerializeField] private GameObject buyButton;
    [SerializeField] private GameObject selectButton;
    [SerializeField] private Animator anim;

    private void SetupSkinInfo()
    {
        skinPurchased[0] = true;

        for (int i = 1; i < skinPurchased.Length; i++)
        {
            bool skinUnlocked = PlayerPrefs.GetInt("SkinPurchased" + i) == 1;

            if (skinUnlocked)
                skinPurchased[i] = true;
        }

        bankText.text = PlayerPrefs.GetInt("TotalFruitsCollected").ToString();


        selectButton.SetActive(skinPurchased[skin_Id]);
        buyButton.SetActive(!skinPurchased[skin_Id]);

        if (!skinPurchased[skin_Id])
            buyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Price: " + priceForSkin[skin_Id];

        anim.SetInteger("skinId", skin_Id);
    }

    public bool EnoughMoney()
    {
        int totalFruits = PlayerPrefs.GetInt("TotalFruitsCollected");

        if(totalFruits > priceForSkin[skin_Id])
        {
            totalFruits = totalFruits - priceForSkin[skin_Id];

            PlayerPrefs.SetInt("TotalFruitsCollected", totalFruits);

            AudioManager.instance.PlaySFX(5);
            return true;
        }

        AudioManager.instance.PlaySFX(6);
        return false;

    }

    public void NextSkin()
    {
        AudioManager.instance.PlaySFX(4);

        skin_Id++;

        if (skin_Id > 3)
            skin_Id = 0;

        SetupSkinInfo();
    }
    public void PreviousSkin()
    {
        AudioManager.instance.PlaySFX(4);

        skin_Id--;

        if (skin_Id < 0)
            skin_Id = 3;

        SetupSkinInfo();
    }
    public void Buy()
    {
        if (EnoughMoney())
        {
            PlayerPrefs.SetInt("SkinPurchased" + skin_Id, 1);
            SetupSkinInfo();
        }
        else
            Debug.Log("NotEnoughMoney");
    }
    public void Select()
    {
        PlayerManager.instance.chosenSkinId = skin_Id;
    }
    public void SwitchSelectButton(GameObject newButton)
    {
        selectButton = newButton;
    }
    private void OnEnable()
    {
        SetupSkinInfo();
    }
    private void OnDisable()
    {
        selectButton.SetActive(false);
    }
}
